## Zbiory
set PR; # Produkty
set T; # Typy maszyn
set P; # Prawdopodobienstwa
set M; # Miesiace
set K; # Kryteria

## Parametry
param iloscMaszyn {t in T};
param czasyProdukcji {t in T, pr in PR};
param ceny {p in P, pr in PR};
param prawdopodobienstwa {p in P};
param sredniaCena {pr in PR} = sum {p in P} ceny[p, pr] * prawdopodobienstwa[p];
param pesymistycznaCena {pr in PR} = min {p in P} ceny[p, pr];
param godzinWMiesiacu;
param ograniczeniaSprzedazy {m in M, pr in PR};
param kosztPrzechowywania;

# Metoda punktu odniesienia
param a {k in K}; # Punkt aspiracji
param lambda; # Stala skalujaca
param beta; # Krok
param eps; # Bardzo mala liczba

## Zmienne
var wlaczoneMaszyny {m in M, t in T} >=0; # Ile maszyn jest wlaczonych w kazdym miesiacu
var stanMagazynu {0..3 , pr in PR} integer >=0 <=200; # Stan magazynu
var zmianaMagazynu {m in M, pr in PR} integer;
var iloscWyprodukowana {m in M, pr in PR} integer >=0;
var sprzedaz {m in M, pr in PR} >=0 integer;
var koszt {m in M} >= 0;
var przychod {m in M};
var odchylenie {m in M} >=0;

# Metoda punktu odniesienia
var y {k in K} >= 0; # Zbior kryteriow (zysk, ryzyko)
var v; # Zmienna pomocnicza
var z {k in K}; # Zmienna pomocnicza

## Ograniczenia
s.t. stanMagazynuPoMarcu {pr in PR}:
	stanMagazynu[3, pr] = 50;
	
s.t. stanMagazynuNaPoczatkuStycznia {pr in PR}:
	stanMagazynu[0, pr] = 0;

s.t. stanMagazynuPoStyczniu {m in 2..3, pr in PR}:
	stanMagazynu[m, pr] >= 20;
	
s.t. funkcjonowanieMaszyn {m in M, t in T}:
	wlaczoneMaszyny[m, t] <= iloscMaszyn[t];

s.t. konserwacja {t in T}:
	sum{m in M} (wlaczoneMaszyny[m, t]) <= iloscMaszyn[t]*3 - 1;
	
s.t. produkcja {m in M, pr in PR}:
	iloscWyprodukowana[m, pr] = sum {t in T} ( czasyProdukcji[t, pr] * wlaczoneMaszyny[m, t] ) * godzinWMiesiacu;
	
s.t. magazyn {m in M, pr in PR}:
	stanMagazynu[m, pr] = stanMagazynu[m-1, pr] + zmianaMagazynu[m, pr];
	
s.t. miesiecznaSprzedaz {m in M, pr in PR}:
	sprzedaz[m, pr] = iloscWyprodukowana[m, pr] - zmianaMagazynu[m, pr];
	
s.t. ograniczenieSprzedazy {m in M, pr in PR}:
	sprzedaz[m, pr] <= ograniczeniaSprzedazy[m, pr];
	
s.t. miesiecznyKoszt {m in M}:
	koszt[m] = sum {pr in PR} stanMagazynu[m, pr] * kosztPrzechowywania;
	
s.t. miesiecznyPrzychod {m in M}:
	przychod[m] = sum {pr in PR} (sprzedaz[m, pr] * sredniaCena[pr]);

s.t. miesieczneRyzyko {m in M}:
	odchylenie[m] = sum {pr in PR} (sprzedaz[m, pr] * (sredniaCena[pr] - pesymistycznaCena[pr]));

s.t. zysk:
	y[1] = sum {m in M} (przychod[m]-koszt[m]);

s.t. ryzyko:
	y[2] = sum {m in M} odchylenie[m];


# Metodu punktu odniesienia
s.t. warunek1 {k in K}:
	v <= z[k];

s.t. warunek2Zysk:
	z[1] <= beta*lambda*(y[1]-a[1]);

s.t. warunek2Ryzyko:
	z[2] <= beta*lambda*(a[2]-y[2]);

s.t. warunek3Zysk:
	z[1] <= lambda*(y[1]-a[1]);

s.t. warunek3Ryzyko:
	z[2] <= lambda*(a[2]-y[2]);
	

# Funkcja celu
maximize zyskTotal: v + eps*(z[1] - z[2]);